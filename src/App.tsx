import React, { useState } from "react";
import logo from "./logo.svg";
import style from "./App.module.css";

export const replaceCamelCaseWithSpaces = (colorName: string): string => {
  return colorName.replace(/\B([A-Z])\B/g, " $1");
};

function App() {
  const [color, setColor] = useState<string>("MediumVioletRed");
  const [cbSelected, setCbSelected] = useState<boolean>(false);
  const currentColor =
    color === "MediumVioletRed" ? "MidnightBlue" : "MediumVioletRed";

  return (
    <div>
      <button
        onClick={() => setColor(currentColor)}
        // className={style.button}
        style={{
          backgroundColor: cbSelected ? "gray" : currentColor,
          color: "white",
          padding: 20,
        }}
        data-testid="app-single-button"
        disabled={cbSelected}
      >
        change to {replaceCamelCaseWithSpaces(color)}
      </button>
      <input
        type="checkbox"
        name="checkBX"
        id="cbGeneral"
        data-testid="checkbox1"
        onChange={(e) => {
          setCbSelected(e.target.checked);
        }}
      />
    </div>
  );
}

export default App;
