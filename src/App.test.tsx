import { render, screen } from "@testing-library/react";
import { click } from "@testing-library/user-event/dist/click";
import App from "./App";

describe("CHECKS INITIAL COLORS AND TEXT", () => {
  let button: HTMLElement;
  beforeEach(() => {
    render(<App />);
    button = screen.getByTestId("app-single-button");
    // console.log(button);
  });
  test("button should has the correct initial color", () => {
    expect(button).toHaveStyle({ backgroundColor: "MidnightBlue" });
  });

  test("button should have correct initial text", () => {
    expect(button.textContent).toBe("change to Medium Violet Red");
  });
});

describe("CHECKS COLOR AND TEXT AFTER CLICK BUTTON", () => {
  let button: HTMLElement;
  beforeEach(() => {
    render(<App />);
    button = screen.getByTestId("app-single-button");
    // console.log(button);
  });
  it("button should has different color than initial when click it", () => {
    expect(button).toHaveStyle({ backgroundColor: "MidnightBlue" });
    click(button);
    expect(button).toHaveStyle({ backgroundColor: "MediumVioletRed" });
  });

  it("button should has different text value than initial when click it", () => {
    expect(button.textContent).toBe("change to Medium Violet Red");
    click(button);
    expect(button.textContent).toBe("change to Midnight Blue");
  });
});

describe("CHECK COLOR WHEN BUTTON CHECK IS CLICKED", () => {
  let button: HTMLElement;
  let checkButton: HTMLElement;
  beforeEach(() => {
    render(<App />);
    button = screen.getByTestId("app-single-button");
    checkButton = screen.getByTestId("checkbox1");
    // console.log(button);
  });
  test("button should be disable and bg color should be gray when check button is checked.", () => {
    expect(button).toHaveStyle({ backgroundColor: "MidnightBlue" });
    click(checkButton);
    expect(checkButton).toBeChecked();
    expect(button).toHaveStyle({ backgroundColor: "gray" });
    expect(button).toBeDisabled();
  });
  test("button should be enable back and bg color should be MidnightBlue when check button is unchecked.", () => {
    expect(button).toHaveStyle({ backgroundColor: "MidnightBlue" });
    click(button);
    expect(button).toHaveStyle({ backgroundColor: "MediumVioletRed" });
    click(checkButton);
    expect(checkButton).toBeChecked();
    expect(button).toHaveStyle({ backgroundColor: "gray" });
    expect(button).toBeDisabled();
    click(checkButton);
    expect(checkButton).not.toBeChecked();
    expect(button).toBeEnabled();
    expect(button).toHaveStyle({ backgroundColor: "MediumVioletRed" });
  });
});
