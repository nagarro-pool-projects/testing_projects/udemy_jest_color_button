import React from "react";
import { render, screen, fireEvent, getByRole } from "@testing-library/react";
import App, { replaceCamelCaseWithSpaces } from "./App";
// ===============
test("Single button has a correct initial color", () => {
  render(<App />);
  const button = screen.getByRole("button", {
    name: /change to Medium Violet Red/,
  });
  // const button = screen.getByTestId("app-single-button");
  expect(button).toHaveStyle({
    backgroundColor: "MidnightBlue",
  });
});
// ===============
test("Single button has a correct initial text", () => {
  render(<App />);
  const button = screen.getByRole("button", {
    name: /change to Medium Violet Red/,
  });
  fireEvent.click(button);
  expect(button).toHaveStyle({
    backgroundColor: "MediumVioletRed",
  });
  expect(button.textContent).toBe("change to Midnight Blue");
});
// ==============
test("button should be disable when checkbox has been checked", () => {
  render(<App />);
  const button = screen.getByRole("button", {
    name: /change to Medium Violet Red/,
  });
  const checkbox = screen.getByRole("checkbox");
  expect(button).toBeEnabled();
  expect(checkbox).not.toBeChecked();

  fireEvent.click(checkbox);
  expect(button).toBeDisabled();
  expect(checkbox).toBeChecked();

  fireEvent.click(checkbox);
  expect(button).toBeEnabled();
  expect(checkbox).not.toBeChecked();
});

test("click checkbox and check if the button bg is gray, click again and verify if btn bg color is blue", () => {
  render(<App />);
  const checkbox = screen.getByRole("checkbox");
  const btn = screen.getByRole("button", {
    name: "change to Medium Violet Red",
  });

  fireEvent.click(checkbox);

  expect(btn).toBeDisabled();
  expect(btn).toHaveStyle({
    backgroundColor: "gray",
  });

  fireEvent.click(checkbox);
  expect(btn).toBeEnabled();
  expect(btn).toHaveStyle({
    backgroundColor: "MidnightBlue",
  });
});

test("click button to change it to red, then click the checkbox and expect btn bg will be gray, then click again the checkbox and verify if btn bg color is red", () => {
  render(<App />);
  const checkbox = screen.getByRole("checkbox");
  const btn = screen.getByRole("button", {
    name: "change to Medium Violet Red",
  });

  fireEvent.click(btn);
  fireEvent.click(checkbox);

  expect(btn).toBeDisabled();
  expect(btn).toHaveStyle({
    backgroundColor: "gray",
  });

  fireEvent.click(checkbox);

  expect(btn).toHaveStyle({
    backgroundColor: "MediumVioletRed",
  });
  expect(btn).toBeEnabled();
});

describe("spaces before camel-case capital letters", () => {
  test("works for no inner capital letters ", () => {
    expect(replaceCamelCaseWithSpaces("Red")).toBe("Red");
  });

  test("works for one inner capital letter", () => {
    expect(replaceCamelCaseWithSpaces("MidnightBlue")).toBe("Midnight Blue");
  });

  test("works for multiple inner capital letters ", () => {
    expect(replaceCamelCaseWithSpaces("MediumVioletRed")).toBe(
      "Medium Violet Red"
    );
  });
});
